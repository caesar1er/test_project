# test_project

### un projet de test => Notre objectif est de partager un git sans faire n'importe quoi

## Attentes

* je n'avais pas d'attentes détaillées vis à vis de la formations car je ne connaissais pas le DevOps

## Objectifs

* Découvrir de nouveaux outils pour apprendre à mieux répondre aux besoins digitaux
* Etre prêt pour aborder serainement les projets qui me seront confiés

## l'Atteinte des objectifs

- [ ] Comprendre ce qu'est le DevOps
- [ ] Comprendre les différents outils qui le compose
- [ ] Comprendre l'articulation des différents outils et leur place dans le WorkFlow DevOps

## Commentaires

# Personnel

* la compréhension du besoins et la mise en place de la solution technique qui en découle est un des workflow qui me passione

# Autres 
